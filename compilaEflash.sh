#!/bin/bash

COM=/dev/ttyACM0

echo "(suppone board agganciata a $COM)"

if
 test $# -ne 1
then
 echo Usage: $0 '<sketch dir>'
 exit 1
fi

arduino-cli compile --fqbn arduino:avr:uno $1
arduino-cli upload -p $COM --fqbn arduino:avr:uno $1
