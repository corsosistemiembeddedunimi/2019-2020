# Progetti d'esame

Pensate a progetti che privilegino l'hardware (sensori e attuatori, interfacciamento) rispetto al software: cose come app, siti web, altri sistemi "grossi" devono essere solo corollari (e sono /out of scope/ rispetto al corso). Mi interessa che vi "scontriate" col mondo fisico.
Altra cosa importante: l'uso di sistemi /closed-loop/ cioè con un minimo di feedback.


## Modalità

Gruppi sono possibili, ma limitatamente a due studenti (oltre si rischia dispersione delle competenze).

Ogni studente o gruppo *deve* dichiararsi su questa pagina (seguire il template), alla sezione "in itinere".

Template:

	Nome progetto

	Autori: nome1 cognome1, nome2 cognome2

	Descrizione: breve descrizione (hw/sw) del progetto

	Link a repo: creare un repository git (su un server tipo github o gitlab o altro pubblico) e linkarlo qui

	Licenza scelta: (per la scelta consultare ad es. https://www.gnu.org/licenses/licenses.html)
	Attenzione: deve essere una licenza LIBERA, cfr. anche https://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses
	
	Data *indicativa* di presentazione: basta il mese


## Idee (alcune invero balzane, ma si fa anche per giocare)

* Creare nuovi (o vecchi) strumenti di misura digitali, esempio una bilancia a "stadera" che si... autobilancia (sembra un gioco di parole)
* Sperimentare con "encoder ottici" vari (dischi di cartone su cui disegnare pattern da leggere mediante LED + fotosensore)
* Apparecchio di "voto" per pubblico di programmi televisivi
* Telecomando TV programmabile (nel senso di: segue un calendario di programmi su cui piazzare la TV, magari accendendo anche altri sistemi, tipo casse esterne etc.) via MQTT o altro
* Robottini vari che esplorano un ambiente, anche molto banali (tipo con "baffi" a microswitch per saggiare gli ostacoli)
* Piano che si autostabilizza (perpendicolare alla gravità) mediante accelerometri
* Strumento "musicale" (percussioni? o simili) comandato via MIDI/OSC
* Ventilatore che si adatta all'umidità della pelle
* Mappare una stanza usando il "time of flight" sensor di ST
* Monopattino elettrico con recupero energia in frenata
* Sensori ambientali non banali: combinare più sensori "banali" per misurare una proprietà di un ambiente. Es. localizzazione indoor di un device, contare persone in un ambiente, ...
* Inseguitore solare (me lo ha proposto uno di voi, segnalo qui per dare idee ulteriori)
* Integrazione di sensori in sistemi domotici, spiego: oggi esistono molti sistemi (anche liberi, cfr. openHAB e HomeAssistant) di gestione domotica, tali sistemi "parlano" già molti protocolli standard (MQTT, etc.) e sono capaci di interfacciarsi con sensori "standard". Un buon esercizio potrebbe essere quello di realizzare un sensore e renderlo "integrabile" in questi sistemi, cioè dotarlo della capacità di parlare facilmente con tali sistemi, in modo da non dover costringere gli utilizzatori a scrivere ulteriore software, ma solo scrivere un file di configurazione.
* Installazioni (anche interattive) "artistiche": animazioni d'acqua (https://www.youtube.com/watch?v=gusJeslMbLc), luce (https://www.youtube.com/watch?v=KmjP5VwuqBw dal minuto 35 circa, ma anche tutto), suono, etc.
* ...

In generale, tanto per partire e superare il "blocco del maker": un sensore, un attuatore, controllo feedback (closed loop).

## In itinere (progetti iniziati, ancora da presentare all'esame)

### Portable Thermostat

Autore: Davide Busolin

Descrizione: Il progetto consiste in uno o più termostati portatili che possono essere posizionati in qualunque punto della casa (in cui ci sia copertura Wi-Fi) in modo tale da avere un controllo della temperatura più fine dove effettivamente è necessario riscaldare, e un'unità fissa che opera sul comando della pompa dei termosifoni (che si trova dove sarebbe posizionato il classico termostato).

Hardware:
Per l'unità fissa si ha:
 - Wemos D1 mini (ESP8266)
 - Relé

Per le unità mobili si ha:
- Wemos D1 mini (ESP8266)
- Termometro DHT11 (Sarebbe forse meglio un DHT22)
- Display OLED SSD1306 128x32
- Tre pulsanti

Link a repo: https://github.com/busolind/PortableThermostat

Licenza scelta: GPLv3

Data *indicativa* di presentazione: 03/21


### you-would-never-hack-a-car

Autore: Luigi Foscari

Descrizione: Si vuole creare un dispositivo in grado di prelevare informazioni da una macchina e di mostrarle all'utente. Un ELM327 collegato tramite OBD-II fornisce i dati del veicolo. Un giroscopio e accelerometro MPU6050 fornisce l'inclinazione laterale e frontale del veicolo. Un ESP32 legge i dati dai sensori tramite Bluetooth genera su Wi-Fi AP un web server, tramite il quale l'utente può accedere ad una dashboard costruita con Vue.js che sfrutta un canale socket con l'ESP32.

**Hardware:**

- ESP32
- ELM327
- MPU6050

https://github.com/lfoscari/you-would-never-hack-a-car

Licenza: MIT

Data di presentazione: 25/01/2021

## Presentati


### Sintuino

Autore: Diego Alberto Iozzia

Descrizione: Il Sintuino è un sintetizzatore basato su Arduino UNO suonato attraverso il movimento della mano davanti al sensore a ultrasuoni. Oltre alla sintesi sonora, permette di decidere l'ottava a cui sarà suonato e implementa un effetto tremolo regolabile, attivabile tramite pulsante, e un pitch shifter dinamico pilotato da un secondo sensore a ultrasuoni.

Hardware

- Arduino UNO x1

- Potenziometro 10k x3

-  Sensore a ultrasuoni HCSR-04 x2

-  Uscita Jack mono 6.35 mm x1

-  Resistore 560 OHM x1

-  Led x1

-  bottone x1

Librerie

-  Volume3

-  NewPing

Link a repo: https://github.com/n0men/Sintuino

Licenza scelta: CC0 1.0 Universal

Data *indicativa* di presentazione: dicembre


### Bluetooth Smart Car

Autore: Manuel Savà

Descrizione: il progetto consiste nella realizzazione di una macchinina comandabile via Bluetooth da qualsiasi Device compatibile con modulo Bluetooth HC-06. La Smart car presenta due modalità di utlizzo, switchabili in qualsiasi istante dal device collegato: La prima denominata "guida libera", selezionata di default all'accensione, consiste nel comandare la macchinina tramite il pad virtuale del device, decidendo liberamente la velocità di movimento e la direzione da farle prendere, mentre la seconda consiste in una sorta di "pilota automatico", facendola muovere autonomamente e fermandosi una volta al cospetto di un qualsiasi ostacolo, cambiando successivamente direzione verso la via più libera. La funzionalità del fermarsi dinanzi a un ostacolo è presente anche nella guida libera, ma in questo caso la Smart Car si limiterà solo a stoppare i motori, lasciando pieno controllo all'utente su quale strada percorrere.  

**Hardware e materiale utilizzato:**

- Aruino uno
- Modulo Bluetooth HC-06
- DC Motor Shield L293D
- 2 Batterie 18650 da 3.7V
- Porta Batterie con Switcher integrato
- Sensore a ultrasuoni HC-SR04
- 4 Motori DC con ruote annesse
- Chassis a 2 strati in plastica

Link a repo:https://github.com/manuelsava/Sistemi-Embedded

Licenza scelta: GPLv3

Data *indicativa* di presentazione: 12/2020


### RandomicDice

Autore: Giacomo Andreata

Descrizione: un dado che al lancio, grazie ad un accelerometro, capisce di essere stato lanciato o agitato e genera un numero random e lo mostra sul display una volta che si è fermato. Il range può essere preimpostato ma con un massimo che va da 0 a 9.

link a repo: https://github.com/GiacomoAndreata/RandomicDice

Licenza: GPL-3.0

Data *indicativa* di presentazione: SETTEMBRE











### Smart Remote

*Nome Progetto*: Smart Remote

*Autore*: Alessandro Mascaretti

*Descrizione*: Il telecomando in questione è basato su ESP32. Grazie all'utilizzo di uno script di shell o di un assistente vocale per smartphone (ancora da valutare quale possa essere la scelta migliore tra le 2),
verranno mandati dei comandi MQTT al telecomando, il quale dopo averli opportunamente interpretati, li trasformera' in segnale a infrarossi per interagire con la TV. Il telecomando sara' inoltre dotato
di una fotoresistenza per il rilevamento della luminosita' ambientale. Qualore l'utente stesse vedendo la TV al buio, il telecomando in automatico mandera' un comando
MQTT in modo che eventuali lampade, collegate allo stesso server MQTT e poste dietro alla TV, verranno accese. Ai fini del progetto le lampade verranno simulate con uno o due led.

*Hardware* (provvisorio, potrebbe variare e/o aumentare in corso di sviluppo):
* ESP32
* IR Sender
* IR Receiver
* Led
* Fotoresistenza

Nota: il circuito verra' saldato su millefori e non lasciato volante su breadboard, in quanto verra' poi anche effettivamente utlizzato in casa.

*Link a repo* : https://gitlab.com/AleMasca/smart-remote (In Allestimento. Verra' modificato e riempito in fase di sviluppo)

*Licenza scelta* : GPLv3














N.B. si lasciano qui per archivio, non aggiungere proposte-progetti dopo questa riga

### DCC COMMAND STATION

Autore: Paolo Calcagni

E-mail: paolo.calcagni@studenti.unimi.it

Repository: [Bit Bucket](https://bitbucket.org/paolino625/dcc-command-station/src/master/)

Licenza scelta: GPLv3

Data di presentazione: Dicembre 2019

Descrizione: utilizzare Arduino per inviare comandi DCC alle locomotive del plastico ferroviario e gestire gli scambi presenti attraverso un'interfaccia web.

Librerie Software Utilizzate:

- Wire: permette la gestione del display LCD.
- Liquid Crystal I2C: permette la gestione del display LCD.
- EEPROM: permette la gestione memoria non-volatile EEPROM.
- Timer One: permette la gestione degli interrupt.
- Keypad: Libreria permette la gestione del tastierino numerico.
- Firmata Marshaller: permette la gestione della communicazione tra microcontroller.

Hardware Utilizzato:

- Arduino Mega
- ESP8266 (ESP-12E Module)
- Tastierino Numerico
- Display LCD I2C
- Logic Level Converter
- L298N
- Sensori infrarossi - TCRT5000
- Trasduttori di corrente
- Relay - OMRON G6DN 5V
- CDU




### ChaseTheSun

Autore: Lorenzo Bini

Descrizione: ChaseTheSun è un piccolo robot con ruote motorizzate che cerca e raggiunge un luogo illuminato direttamente dal sole nelle sue vicinanze. Se rileva di essere nuovamente all'ombra, si rimette in moto per tornare al sole. Può anche usare i sensori di luminosità per rilevare oggetti in avvicinamento e "scappare" da essi. L'hardware è costituito da una base con ruote motorizzate, fotoresistenze inserite in alloggiamenti di cartoncino nero che fanno da sensori di luminosità, un piccolo pannello solare che rileva l'illuminazione diretta da luce solare (le fotoresistenze non funzionano bene per tale scopo), e il controller ESP32. Il software è un loop che controlla il pannello e le fotoresistenze per decidere se e dove spostarsi. La rilevazione dei luoghi illuminati è principalmente basata sul fatto che le superfici illuminate direttamente dal sole registrano sempre valori molto più alti di quelle all'ombra quando "osservate" tramite una fotoresistenza. Gli alloggiamenti in cartoncino nero servono a schermare una fotoresistenza da fonti di luce diverse dalla superficie del terreno, ad esempio il cielo o il paesaggio intorno; in pratica si dà al sensore un "campo visivo" ben limitato, nonostante la fotoresistenza di per sè non ne abbia uno (questo aspetto è già stato testato all'aperto). Il progetto è solo in fase iniziale, per ora.

Repository: https://gitlab.com/ellebi/chasethesun (in Inglese)

Licenza scelta: GPLv3





### Interactive LED matrix

*Autore*: Mirko Milovanovic

*Descrizione*: Creazione di una matrice LED 17x17 controllata da un ESP32.
La matrice è (semi)interattiva, potendo essere controllata in 2 modi: attraverso il protocollo Artnet o E131 è possibile inviare animazioni e giochi di luce da un controller
esterno in modo wireless; oppure, creando un sensore "touch" utilizzando 2 sensori ad ultrasuoni per gli assi X e Y è possibile interagire con un minimo di UI (esempio sveglia con informazioni meteo, etc).

Vorrei pensare un attimo su quali altri sensori potrei integrare o quali altre funzionalità creare dato che penso sia un attimo semplice come progetto (ergo sono ben accette idee su come migliorare il tutto) (atrent: le funzionalità che hai citato sono sufficienti per un progetto d'esame, se implementi sia controllo diretto che via rete. se proprio vuoi aggiungere qualcosa metti un accelerometro e sposta un puntoluce sulla matrice in funzione dell'inclinazione)

*Hardware* (provvisorio):

* ESP32
* 2x ultrasonic sensor HY-SRF05
* SK9822 RGB LED strip
* Trasformatore 5V, 100W

*Link a repo* : [https://gitlab.com/kobimex/interactive-led-matrix](https://gitlab.com/kobimex/interactive-led-matrix) (in allestimento)

*Licenza scelta* : GPLv3

*Data di presentazione*: 31 luglio '19



### Termostato Nest fai da te

Autore: Alessio Medda

Descrizione: Termostato domotico basato su ESP-32 in grado di regolare di interagire con la caldaia.
È controllabile sia attraverso i comandi manuali sia interfacciandosi con google assistant e fornire informazioni sulla temperatura nell'abitazione .
Viene implementato un PID per migliorare la regolazione della temperatura.

(commento atrent: il Nest "impara" la programmazione settimanale man mano che lo usi, vorresti provare a implementare una cosa analoga?)

Hardware:
    -DHT11
    -Display Oled AZOLED12864-1000
    -2 bottoni
    -1 potenziometro

Link a repo: https://github.com/alemk96/Termostato

Licenza scelta: GPL 3.0





### ArduinoMicroSynth 

Nome progetto: ArduinoMicroSynth

Autore: Alessandro Chiesi

Descrizione: Intendo realizzare un synth analogico pilotato via MIDI attraverso un Arduino Uno. Il sintetizzatore è costituito da due organi fondamentali:

- un oscillatore costruito con un integratore e un comparatore i quali forniscono rispettivamente onda quadra e triangolare.Inoltre è presente un input, ricevente una tensione in ingresso, la quale sceglierà la frequenza dell'oscillatore.
- un amplificatore costruito con un operazionale che regola l'ampiezza dell'onda generata dall'oscillatore. l'amplificatore riceve anch'esso una tensione d'ingresso per regolare il volume in uscita

Il software si occupa di gestire 3 funzionalità principali:

- gestione dei messaggi MIDI in ingresso
- erogazione della tensione per l'oscillatore ,attraverso il PWM generato da Arduino , secondo il messaggio MIDI ricevuto, opportunamente filtrato.
- erogazione della tensione per l'amplificatore, attraverso un output digitale PWM, filtrato opportunatamente, per ottenere una tensione continua.

La dinamica è regolabile secondo un ADSR implementato via software, attraverso quattro potenziometri:

* Attacco: tempo necessario per raggiungere il volume massimo
* Decadimento: tempo necessario per raggiungere il volume di Sustain
* Sustain: ampiezza a cui il volume rimarrà costante finchè il tasto rimane premuto, dopo il tempo di decadimento
* Rilascio: tempo in cui, una volta rilasciato il tasto, il volume arriverà a zero

Link a repo: https://github.com/Keasys/ArduinoMicroSynth

Licenza scelta: GPLv3
	




### Ardumeteo

*Autore*: Matteo Carlo Giavarini

*Descrizione*: Ardumeteo ha come obbiettivo la realizzazione di una piattaforma integrativa per un impianto di domotica.
L'idea è quella di avere una piattaforma in grado di rilevare valori ambientali indoor come temperatura, umidità, luminosità, pressione ed altitudine per poi condividerli tramite protocollo MQTT con la piattaforma Home Assistant.
Tramite l'interfaccia grafica di Home Assitant si potranno visualizzare i grafici relativi ai dati rilevati, sarà possibile scegliere se controllare le luci interne in maniera manulare tramite l'uso di un potenziometro collegato ad un Arduino, oppure in maniera automatica in base ai valori rilevati dal sensore di luminostà.
Inoltre sempre tramite interfaccia grafica sarà possibile impostare il periodo di lettura dei dati. La logica della gestione dei parametri è implementata su Home Assitant grazie all'utilizzo di trigger ed automazioni che invieranno su topic MQTT gli opportuni comandi.
Per comodità ho scelto di installare il broker MQTT su un PC Linux e non su un raspberry come è consueto fare. I vari sensori ambientali sono stati collegati ad un ESP32 connesso tramite Wifi mentre il potenziometro per il controllo manuale delle luci è stato collegato ad un clone di Arduino Uno connesso con ethernet shield,
mentre le luci interne sono simulate da led collegati ad entrambe le piattaforme.
Ho scelto di utilizzare due dispositivi per sottolineare la possibilità di avere più dispositvi che interagiscono fra di loro.

*Repository*: https://github.com/matteocarlogiavarini/Ardumeteo (Il progetto è completo ma potrei apportare dei cambiamenti)

*Licenza scelta*: GPLv3

(atrent: ok, ricorda che è più importante la parte embedded rispetto alla parte "home assistant"... caveat emptor)


### Smart home

*Autore*: Daniele Coccia

*Descrizione*: Sistema domotico che implementa le funzionalità di:

  - **Sensore temperatura e umidità**
    Tramite un sensore **DHT11** misuro temperatura e umidità ambientale.
    Quando viene rilevato un eccessivo livello di umidità un'automazione di home assistant invia un messaggio mqtt che aziona un relè che accenderà un deumidificatore.

  - **Controllo consumo energetico**
    Quando viene ricevuto tramite mqtt un livello di consumo energetico eccessivo dal misuratore viene fatto suonare un **buzzer** e vengono spente le luci per un quanto di tempo cercando così di ridurre i consumi.

  - **Luci con intensità variabile**
    una **fotoresistenza** determina la luminosità ambientale, l'intensità luminosa delle lampade è regolata in locale tramite lettura dei pin.
    Viene inviato ad homeassistant la quantità di luce ambientale presente nella stanza.

  - **Sistema di allarme**
    sensore di movimento che alla rilevazione di un movimento fa suonare un buzzer, invia messaggio di rilevato movimento ad homeassistant.
    Possibilità di attivare o disattivare il sistema di allarme da homeassistant tramite mqtt.

*Repository*: https://github.com/daniC97/smartHome

*Licenza scelta*: GPLv3



### Braccio raccoglitore

*Autori*: Luca Papparotto, Alessandro Varotto

*Descrizione* 

Il progetto prevede la realizzazione di un braccio meccanico in grado di raccogliere oggetti e spostarli in un cestino secondo due modalità:

- manuale: il braccio viene comandato attraverso l'utilizzo di un joystick; una volta posizionato sull'oggetto e premuto il tasto del joystick il braccio automaticamente preleverà l'oggetto e lo sposterà nella zona designata.

- automatico: il braccio meccanico attraverso il sensore ultrasonico cerca, nel range della sua presa, oggetti da poter raccogliere e buttare nel cestino.

*Hardware e materiale utilizzato:*

- Esp32
- Servo sg90 x5 
- Joystick x1
- Sensore ultrasonico x1
- Led x2
- Microswitch x1
- Interruttore x1
- Millefori x2

*Repository*: https://gitlab.com/lucapap97/esame-sistemi-embedded

*Licenza scelta:* GPLv3



### Termostatino32

*Autore*: Samuel Albani

*Descrizione*: Il progetto consiste in un termostato realizzato con il microcontrollore ESP32.
Viene fatto uso di un termistore NTC per monitorare la temperatura (calcolata con la formula riportata su https://it.wikipedia.org/wiki/Termistore#Equazione_con_parametro_B),
ed un potenziometro per regolare la temperatura desiderata tra 20 e 40 °C;
l'innalzamento della temperatura oltre la somma tra quella desiderata ed una soglia di 2 °C (per evitare continue accensioni e spegnimenti
dovuti a disturbi di lettura) provoca l'accensione della ventola,
la cui velocità è controllata in PWM attraverso un transistor, in base alla differenza tra la temperatura rilevata e quella desiderata;
quando la temperatura rilevata scende sotto quella desiderata, la ventola si ferma.
Lo stato del sistema è indicato, oltre che attraverso dati inviati su seriale, anche da un LED RGB regolato in PWM che si accende di blu
se la temperatura è sufficientemente bassa, mentre sfuma dal verde al rosso man mano che la temperatura rilevata sale oltre quella desiderata,
proporzionalmente alla velocità della ventola.
Inoltre, vengono inviati dati su temperatura attuale, temperatura desiderata e velocità della ventola percentuale ad un broker MQTT sotto il topic "Termostatino32",
attraverso la libreria PubSubClient, ed è possibile impostare la temperatura desiderata anche via MQTT tramite "Termostatino32/imposta <valore_double>" (sottinteso in °C);
per tornare invece al controllo manuale della temperatura desiderata tramite potenziometro basta inviare via MQTT "Termostatino32/imposta " (con payload vuota).

*Repository*: https://gitlab.com/samuel.albani/termostatino32

*Licenza scelta*: GNU GPLv3




### SimSim

Nome progetto: SimSim

Autore: Andrei Ciulpan

Descrizione:

   Sistema di controllo accessi basato su Arduino.
   Il sistema funziona con diverse modalità di riconoscimento tra cui abbiamo:

     - RFID
     - telecomando (con RF receiver)
     - keypad per poter accedere via password

   Il sistema manda i log di accessi tramite una richiesta HTTP ad un database server in locale
   (ho creato anche la parte di back-end, ma non sta sul repo) con un ESP-01
   Il sistema dispone inoltre di un display LCD 16x2

Repository: https://github.com/Jolsty/SimSim

Documentazione su https://github.com/Jolsty/SimSim/wiki * WORK IN PROGRESS *

Licenza scelta: CC BY-ND




### Salvaduino

Autori: D. Bellisario

Descrizione: obiettivo del progetto è la realizzazione di un salvadanaio controllato da Arduino.
Il salvadanaio:
    * accetta monete da 0.50, 1 e 2 EUR;
    * mostra importo e numero di monete su un display 16 x 2 LCD;
    * salva i dati su EEPROM per mantenere le informazioni anche in caso di spegnimento;
    * autorizza il prelievo solo in possesso di un token RFID;

(altre informazioni sul wiki [[https://github.com/DB375237/salvaduino/wiki]] dedicato)


Repository: https://github.com/DB375237/salvaduino

Licenza scelta: CC0 1.0 Universal.



### RadioArduino

Autori: Adriano Cofrancesco

Descrizione: RadioArduino è un progetto che ha lo scopo di emulare una Radio FM. In particolare questa radio può essere pilotata manualmente, con l'uso di un potenziometro per il cambio frequenza, oppure attraverso appositi messaggi via WiFi utilizzando il protocollo MQTT. Basandosi sullo schema publish/subscribe con MQTT RadioArduino può operare nella modalità  Subscriber, in tal caso può solo ricevere messaggi e rispondere al Publisher in maniera automatica a specifiche richieste, oppure come Publisher, in questo caso oltre alle funzionalità del subscriber può cambiare la frequenza a tutti i subscribers, verificare su quale frequenza sono sintonizzati e anche richiederne l'identificativo. Ogni subscriber apparterrà ad un gruppo, inizialmente di default, che potrà essere cambiato in autonomia così da ricevere messaggi da publisher diversi.

Hardware:
  * ESP8266 NodeMCU
  * LCD 16x2
  * 2x potenziometri 10K
  * TEA5767 FM Radio module

Link a repo: https://github.com/adrianocofrancesco/RadioArduino

Licenza scelta: GPLv3



### RemoteControlledGreenhouse

Autore: Marco De Nicolo

Descrizione: Ho realizzato una serra che può essere controllata da qualsiasi dispositivo, connesso alla stessa rete di questa, attraverso il protocollo OSC.
In particolare si può utilizzare un server per gestire, attraverso un'interfaccia grafica, una o più serre e renderle accessibili anche all'esterno della propria rete.
Per alimentare la serra ho utilizzato un power supply di un vecchio fisso, così da poter utilizzare tutti i diversi voltaggi che mi servivano.
La serra è automatizzata, quindi se l'igrometro segna un'umidità del terreno troppo bassa si apre l'elettrovalvola per innaffiare, se la temperatura o l'umidità è troppo alta si azionano le ventole e così via (i livelli minimi e massimi si possono settare).
La serra comunica con un server per settare l'ora e permettere all'utente di aggiungere innaffiature programmate.
La luce può essere automatica, quindi si accende con il buio, oppure manuale.
HW utilizzato:
  * 2 Ventole
  * elettrovalvola
  * led rosso (si può utilizzare una lampada adatta)
  * igrometro
  * sensore umidità e temperatura DHT11
  * sensore livello acqua
  * fotoresistenza
  * relè 4 canali
  * esp8266
  * power supply
  * breadboard, cavi jumper e altri componenti non elettronici

Link a repo: https://github.com/Maerk/RemoteControlledGreenhouse , https://github.com/Maerk/RemoteControlledGreenhouseServer

Licenza scelta: MPL 2.0







### SmartGarden

Autori: Alessandro Gigliotti, Giovanni Reni

Descrizione: in questo progetto, abbiamo realizzato un sistema di gestione intelligente di un piccolo orto o giardino. È presente un impianto d'irrigazione, basato sul controllo di ciò che accade e quindi di come si modifica, l'ambiente circostante. L'impianto entra infatti autonomamente in azione, quando si verificano determinate condizioni (come secchezza del terreno o assenza di pioggia), tenendo costantemente sotto controllo varie informazioni sull'aria, terra, meteo e sullo stato degli strumenti d'irrigazioni utilizzati. L'irrigazione può entrare in funzione, utilizzando una cisterna che raccoglie l'acqua piovana, oppure tramite un impianto idraulico. È altresì possibile attivare l'irrigazione anche manualmente da un utente tramite un pulsante fisico. Le informazioni rilevate tramite i sensori in giardino, vengono inviate e visualizzate su un display, attaccato ad un'altra board, posizionato in un luogo chiuso (ad esempio in casa). Affianco al display, ci saranno anche 4 led, che segnalano la presenza/assenza di pioggia, la presenza/assenza di sole, lo stato del serbatoio pieno o vuoto. Ci sono infine 3 pulsanti fisici, posizionati accanto al display, per azionare manualmente l'irrigazione, per accendere una lampada per illuminare e un tasto per arrestare entrambe queste attività. Inoltre il sistema sfrutta un'applicazione mobile, dalla quale sono consultabili tutte le informazioni lette dai sensori e lo stato della cisterna. Sono presenti inoltre 3 pulsanti virtuali, gemelli di quelli fisici, per azionare gli strumenti del progetto da un dispositivo. Infine l'app invia anche notifiche, relative agli eventi che accadono nel sistema.

Link a repo: https://github.com/GioReni/SmartGarden

Licenza scelta: GPLv3

### The Mouse

Autori: Simone Calcaterra, Matteo Negri

Descrizione: L'idea del progetto è quella di creare un mouse robotizzato, capace di percorrere tragitti prestabiliti secondo misure date.

per fare ciò utilizziamo la tecnologia del mouse per avere un riscontro sulla tratta percorsa in modo tale da compiere tragitti più o meno precisi.

Il software da noi utilizzato è una libreria, la PS/2 mouse presente in internet.
L'hardware da noi utilizzato è:
  * Un mouse PS\2
  * Una board Arduino pro Micro con integrato ATMEGA32U
  * Due motori DC da 3.3 V

Il nostro progetto focalizza molto l'attenzione sulla libreria e sul protocollo PS\2.

Link a repo: https://github.com/mnegri/TheMouse

Licenza scelta: GPLv3



### Macchina "intelligente"

Autori: Magni Andrea & Mercanti Davide

Descrizione: Il progetto che abbiamo deciso di realizzare prevede la costruzione di una "macchina intelligente". La macchina che costruiremo andrà a svolgere svolgere diverse funzioni di valutazione e risoluzione di problematiche.
  * La macchina dovrà riuscire a muoversi in una strada composta da due corsie rimanendo all'interno di una corsia grazie alla presenza di quattro sensori ad infrarossi posizionati nella parte frontale.
  * La presenza di un semaforo per gestire il traffico gestito da una board differente rispetto a quella della macchina. Entrambe le board avranno a disposizione un modulo radio per consentire la comunicazione.  
  * Nel caso in cui la macchina si stia avvicinando ad un semaforo valuti la sua condizione e regoli la velocità di conseguenza.
  * La presenza di possibili ostacoli durante il percorso, i quali verranno individuati da un modulo a ultrasuoni nella parte frontale della macchina.
  * Nel caso in cui la macchina trovi un ostacolo durante il percorso, i quali possono essere considerati fissi (una macchina in panne, lavori in corso) o mobili (dei pedoni che attraversano la strada) valuti il tipo di ostacolo e decida se continuare il percorso o effettuare una manovra di cambio corsia per poi continuare il percorso.

Link a repo: https://github.com/AndreaMagni/SistemiEmbedded

Licenza scelta: GPLv3





### SHARON-Macchina Self Tuning & Path Follower

Autori: Simone Scaravati, Noah Rosa, Stefano Radaelli

Descrizione: Sharon è una macchina controllabile tramite WiFi, grazie all'uso del protocollo OSC.
È in grado di bilanciare automaticamente la potenza dei suoi motori, in modo da poter andare dritta senza la necessità di andare a regolare manualmente la direzione da lei seguita(errore che capita spesso a causa dell'imprecisione dei motori).
Inoltre si interfaccia con un software (che sarà probabilmente disponibile per tutti i s.o.), scritto su Processing, che permetterà di disegnare graficamente un percorso, il quale sarà seguito dalla macchina il più fedelmente possibile.

Ai fini di testing, il progetto si appoggia all'app AndrOSC (disponibile gratuitamente su Play Store), per guidare manualmente la macchina e per farla anche muovere autonomamente grazie ad un sensore di distanza posto nella parte anteriore della carrozzeria.
Per poter utilizzare l'app facilmente mettiamo a disposizione un preset creato da noi, da collocare in un path specifico descritto nel readme.

Link a repo: https://github.com/simoneScaravati/Sharon-Path-Finder

Licenza scelta: GPLv3

### ESP8266 print server

Autore: Gianluca Nitti

Descrizione: print server basato su ESP8266 per aggiungere connettività di rete a stampanti con porta parallela o seriale. Le stampanti con porta parallela possono essere collegate direttamente (occupando 10 GPIO) oppure tramite shift register (usando "solo" 5 pin di I/O). Sono supportati i protocolli di rete [[http://lprng.sourceforge.net/LPRng-Reference-Multipart/appsocket.htm|AppSocket]] ed [[https://en.wikipedia.org/wiki/Internet_Printing_Protocol|IPP]]. Presente anche una coda di stampa sulla memoria flash a bordo dell'ESP8266 per poter accettare altri job anche quando la stampante è occupata, ma attualmente funziona solo se il job di stampa in attesa rientra completamente nella memoria (quando la stampante si libera, prima finisce di ricevere il nuovo job e poi inizia a stamparlo; migliorabile perchè potrebbe iniziare non appena la stampante è libera, facendo quindi spazio sulla flash).

Link a repo: https://github.com/gianluca-nitti/printserver-esp8266/ (il branch develop viene aggiornato più spesso, facendo il merge in master solo quando è completato lo sviluppo di una funzionalità significativa)

Licenza scelta: GPLv3

### DAMS_01 Sintetizzatore digitale

Autore: Marco Colussi

Descrizione: Il DAMS_01 è un sintetizzatore digitale sviluppato in PureData che utilizza dei sensori ambientali per creare una sintesi diversa e unica in ogni luogo in cui viene usato. Dal lato hw vediamo l'impiego di un raspberry PI3 per far girare il software di PureData, un Arduino UNO per la gestione degli input e la comunicazione con il sintetizzatore via firmata_extended, codice Arduino che modifica il già esistente standardFirmata per permettere di falsare letture e la quantità di pin analogici presenti sulla board.
I sensori utilizzati sono:
  - RGB Led;
  - ADXL 345;
  - Touch sensor;
  - LDR;
  - DHT11;
  - Analog Thumb Joystic;
  - Multiplexer CD4067BE;
  - 5x potenziometri 10kΩ;
  - 5x bottoni;


Link a repo: https://github.com/warpcut/DAMS_01

Licenza scelta: GPLv3

### SolarTracker

Autore: Mirco Gnuva

Descrizione: Sistema in grado di modificare l'inclinazione di un pannello fotovoltaico su due assi tramite servo motori in base alla posizione relativa del sole. La posizione relativa del sole viene determinata utilizzando 4 celle fotovoltaiche che si muovono insieme al pannello principale. Il sistema è inoltre in grado di caricare batterie di tipo 18650 Li-ion e di determinarne lo stato di carica. Il tutto viene controllato tramite un ESP32 (montato su Lolin 32). La logica di controllo è realizzata in Micropython (porting di Python).
Componenti hardware:
  - 2 display oled I2C;
  - Step-down LM2596;
  - Step-up (modello non disponibile);
  - 4 celle fotovoltaiche policristalline (modello non disponibile);
  - Pannello fotovoltaico 5.2W 12V policristallino (modello non disponibile);
  - Bilanciatore di carica/scarica TP4056;
  - Relè 3.3V (modello non disponibile);
  - 2 Servo motori MG90S;
  - Resistori (modello non disponibile);
  - Lolin 32;
In caso di necessità i link di ogni componente sono disponibili su repo(Creazione del README in corso) o tramite mail

Link a repo: https://github.com/Wasp971/SolarTracker

Licenza scelta: GPLv3

### RaspberryCreate

*Autore*: Davide D'Ascenzo, Andrea Scipioni

*Descrizione*: RaspberryCreate è una panoramica dei dispositivi Raspberry in ambiente embedded, con particolare attenzione all'utilizzo della piattaforma Arduino Create.

*Repository*: https://github.com/Kidara/RaspberryCreate

*Licenza scelta*: GPLv3


### Libramentum

*Autore*: Edoardo Ferrari

*Descrizione*: Libramentum è un telecomando al quale è stato collegato un piano movente. Ogni volta che si proverà a roteare il telecomando a destra
o a sinistra rispetto all'asse dell'hardware, il piano verrà stabilizzato e resterà sempre parallelo al terreno. Dati informativi riguardanti la stabilità
del piano verranno inviati tramite connessione MQTT ad un server hostato su https://www.cloudmqtt.com/

*Repository*: https://github.com/normaloide/Libramentum (in aggiornamento)

*Licenza scelta*: GPLv3



### F1 Timer

*Autore*: Mauro Mastrapasqua

*Descrizione*: F1-Timer è un sistema di cronometraggio wireless in grado di misurare i tempi di percorrenza di piu' macchine su un circuito di gara. E' basato sul probing wifi da parte delle macchine verso un'antenna comune presente nel circuito. Un server si occupa di elaborare questi dati e di stendere una classifica delle macchine in gara, oltre che a gestire altri componenti/servizi nel circuito: notifica tramite display della macchina che ha fatto il giro piu' veloce, gestione delle luci di gara (semafori di partenza, luci per segnalare giro veloce ecc...) e infine il sistema ridondante di cronometraggio basato su fotocellula. Il server quindi si occupera' di confrontare i dati acquisiti dai sensori e quelli acquisiti dalle macchine, inviati via wifi. Tutti i dati raccolti vengono messi a disposizione tramite un sito web.

*Repository*: https://github.com/mmastrapasqua/f1timer

*Licenza scelta*: 0BSD


### ArduinoAnnaffiaPiante

*Autore*: Michele Miglio

*Descrizione*: Sistema per monitorare l'umidità di un vaso (in sezioni) e avviare/aumentare o fermare/ridurre la potenza di una pompa per l'acqua così da mantenere l'umidità del vaso sempre in un range scelto tramite prove empiriche.

**Hardware**

*  Arduino Uno: [https://store.arduino.cc/arduino-uno-rev3](https://store.arduino.cc/arduino-uno-rev3)
*  Sensore umidità FC-28: [https://www.robotstore.it/Sensore-di-Umidità-del-suolo](https://www.robotstore.it/Sensore-di-Umidità-del-suolo)
*  Led
*  Cavi
*  Breadboard

Link a repo: [https://gitlab.com/michelemiglio/arduinoannaffiapiante](https://gitlab.com/michelemiglio/arduinoannaffiapiante)

Licenza scelta: GNU General Public License v3.0

Data di presentazione: 11 febbraio 2019

### Allarme

*Autore*: Roberto Antoniello

*Descrizione*: consiste nel simulare un rilevatore di movimento tramite l'uso di sensori di prossimità ad ultrasuoni. Dopo un iniziale monitoraggio dell'ambiente, viene definita per ogni sensore la situazione tipica
in seguito ad un certo numero di misurazioni. Una volta che vengono definiti questi valori, i sensori effettueranno misurazioni periodicamente e nel caso in cui viene notato un cambiamento anormale scatterà l'allarme.
In caso di violazione un buzzer inizierà a suonare e verrà inviata una segnalazione via MQTT. Per disattivare l'allarme sarà richiesto un codice di sblocco e se corretto, il sistema riprenderà con le misurazioni periodiche.

*Link a repo*: https://gitlab.com/roberto.antoniello/progetto_allarme_sistemi_embedded (in aggiornamento)

*Licenza scelta*: GPLv3

### Citofono IOT

*Autori* : Alessandro Sala, Federico Burlon

*Descrizione* : Si tratta di un citofono che comunica con i residenti tramite il servizio cloud-based di messaggistica istantanea Telegram grazie all'utilizzo del modulo ESP8266. Sono previsti due modalità d'uso.
* ONLINE: permette ai residenti di registrarsi al citofono in pochi passi e quindi di ricevere le notifiche quando viene composto il loro interno. Infine, ricevuta una richiesta di apertura, gli utenti possono decidere se aprire o meno il cancello direttamente tramite chat Telegram.
* OFFLINE: il citofono si avvierà in modalità offline qualora non dovessere riuscire a connettersi al WiFi configurato. In questa modalità è comunque possibile aprire il cancello tramite l'immissione di un codice segreto. 

All'installazione viene utilizzata una modalità speciale che permette all'utente di scegliere tra le reti WiFi disponibili quella con cui verrà configurato il citofono da quel momento in poi. Una volta effettuata questa operazione, il citofono cercherà sempre di riconnettersi a tale WiFi anche in seguito allo spegnimento dello stesso. 

*Link a repo* : https://gitlab.com/alessandro14/progettosistemiembedded (almost done)

*Licenza scelta* : GPLv3

### Smart Car 

*Nome Progetto*: John Wick

*Autori*: Boris Coteata, Alex Vecchi

*Descrizione*: Il progetto che abbiamo scelto di presentare riguarda la costruzione di un'auto a 4 ruote con un "armamento" sulla parte superiore dell'auto, l'auto
potrà essere comandata tramite un controller (joystick) che verrà anch'esso costruito da noi e con il quale si potrà anche controllare l'arma.
Inoltre premendo un tasto sul joystick l'auto non riceverà più i comandi dal controller ma inizierà a muoversi verso una direzione, incontrando un ostacolo cercherà di evitarlo proseguendo verso la stessa direzione iniziale oppure cambiandola.
	
*Link a repo*: https://github.com/alexvecchi/SEProject

*Licenza scelta*: GNU GPLv3

### SmartBlind

*Autore*: Luca Ghirotto

*Descrizione*: SmartBlind è un controller per tapparelle integrato con la piattafoma HAP (https://developer.apple.com/homekit/) di Apple per l'IoT. Lo scopo del progetto sarebbe il poter controllare il funzionamanto di tapparelle (o tende elettriche) dai dispositivi Apple direttamente dall'applicazione Home, cosi da non dover creare un'App ad-hoc.
Il controller riceverà le istruzioni via WiFi e azionerà un motore bifase per tirare su o giù le tapparelle e controllerà gli eventuali input manuali dell'utente mettendosi a valle degli interruttori a muro e quindi intecettando gli azionamenti degli stessi.

*Repository*: https://github.com/Ghir8/SmartBlind

*Hardware*:
* ESP 32
* 2 Relè da 220V
* 2 Step-down da 220V a 3V

*Licenza scelta*: GPLv3


### Telecomando condizionatore WiFi programmabile

*Nome Progetto*: Telecomando ARC433 WiFi

*Autore*: Federico Dossena

*Descrizione*: Creare un dispositivo in grado di controllare un vecchio condizionatore Daikin Inverter ricevendo comandi da un'interfaccia web, oppure eseguendo autonomamente uno script LUA inserito dall'utente nell'interfaccia web

*Hardware*:
* Wemos D1 Mini (ESP8266)
* Emettitore IR 940nm
* RTC
* Sensore BME280 (temperatura, umidità, pressione)
* Transistor, LED, resistenze varie

*Link a repo* : https://github.com/adolfintel/espac (leggermente diverso dalla versione presentata all'esame per togliere alcune parti in italiano. La versione presentata all'esame è disponibile quì: https://fdossena.com/espac/relazione.7z)

*Licenza scelta* : GPLv3

### Macchina del Caffè controllabile via WiFi

*Nome Progetto*: CAFFETTino

*Autore*: Andrea Folci

*Descrizione*: Fare un operazione di Reverse Engineering su una macchina del caffè domestica Gaggia Unica per sostituirne il circuito di controllo, aggiungendo un sensore di temperatura e di livello dell'acqua consultabili da Client MQTT.

*Hardware*:
* Wemos D1 Mini (ESP8266)
* Arduino Uno R3
* Termistore TMP36
* Sensore ad Ultrasuoni HC-SR04
* Relè 5V KY-019
* Jumper Wires, Trasformatori 220v -> 24v e 220v -> 5v, Bottoni

*Link a repo* : https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/src/master/

*Licenza scelta* : GPLv3

### Razorback tank

*Nome Progetto*: Razorback tank

*Autore*: Davide Tricella

*Descrizione*: Il progetto consiste in un modellino di un mezzo cingolato controllato tramite Bluetooth, sarà dotato di una certa capacità di muoversi in autonomia, tramite sensore di distanza e bussola. Con comandi inviati da uno smartphone sarà possibile muoverlo direttamente, oppure istruirlo su un percorso da seguire tramite istruzioni in stile "Turtle Graphics".

*Hardware*:
* Arduino Uno R3 con modulo di espansione
* Doppio Driver Motori IRF3205
* Modulo Bluetooth HC-05
* Led vari
* 2 Transistor NPN
* Bussola elettronica GY-271
* Sensore ad ultrasuoni HCSR04
* Modulo MP3 DFMiniPlayer
* Mini Speaker da 3W
* Micro SD

*Link a repo* : https://github.com/dadit97/Razorback-Project (In completamento)

*Licenza scelta* : GPLv3


### AiRPianino

*Nome Progetto*: AiRPianino

*Autore*: Luca Garufi

*Descrizione*: AiRPianino è un progetto che simula uno strumento musicale virtuale ispirato a quello del Pianoforte. L'idea è quella di simulare una tastiera virtuale "in aria" attraverso l'utilizzo di 6 anelli, la propria mano e la sensoristica per permettere di suonare fino a 5 note, anche in contemporanea, su una scala preimpostata. Inoltre, con l’uso di un sesto sensore, è possibile agire anche sulla modulazione per apportare modifiche in real time sui filtri del suono. L’hardware usato è composto da: 6 sensori MPU6050, 1 Display LCD 20x4 tramite I2C,  Seriale MIDI.
A livello software, per la comunicazione con le periferiche, si utilizza il protocollo MIDI e attraverso dei calcoli e l’analisi dei valori ricevuti dai sensori, si generano i rispettivi messaggi.

*Link a repo*: https://gitlab.com/sirluke/midi_mpus

*Licenza Scelta*: GPLv3

### AirController

*Nome Progetto*: AirController

*Autore*: Valerio Cislaghi

*Descrizione*: AirController è un sistema intelligente di controllo automatico per condizionatori che hanno rotto (o non hanno) un sensore di temperatura (portano la temperatura/umidità a livelli minimi possibili).
La gestione del condizionatore avviene attraverso segnali infrarossi.
AirController riceve in input un indice di calore come target (HUMIDEX) e il suo obbiettivo sarà mantenere costante questo indice.
La gestione del condizionatore si basa su diversi fattori:
- Temperatura/Umidità esterna (chiamate API a openWeather).
- Temperatura/Umidità interna (DHT22/DHT11).
- Numero di persone nella stanza (AM312).

Altri componenti utilizzati:
- 2 diodi a infrarossi a LED (emissione e ricezione).
- Display che mostra la temperatura interna.

Tutti i dati (temperatura, unimdità, indice di calore, conta persone ecc...) saranno inviati ad un server MQTT per la sincronizzazione dei vari ESP8266 e per la raccolta dei dati, per stabilire (attraverso analisi) se effettivamente c'è un risparmio di corrente e di salute.

*Link a repo*: https://github.com/Jaivra/AirController

*Licenza Scelta*: GPLv3

### BotTender

Autore: Mario Petruccelli

Descrizione: BotTender è un cocktail maker fatto con arduino che attraverso delle pompe peristaltiche create con dei motori passo-passo nema 17 ti permette di scegliere un drink da una lista predefinita.

La quantità versata viene scelta dall'utente in base a quanti ml di liquido vuole nel suo bicchiere, e il cocktail viene servito rispettando le proporzioni della ricetta.

Link a repo: https://github.com/Mafidyuz/BotTender

Licenza scelta: Creative Commons Attribution 4.0 International

Data: 09/2020


### Bluetooth Quadcopter 

Autore: Vladislav Chircu

Descrizione: Il progetto consiste nella realizzazione di un quadrilatero che sarà in grado di restare in equilibrio autonomamente usando un giroscopio e accelerometro. Il controllo del quadrilatero sarà effettuato tramite un applicazione che si collegherà con il drone tramite il blutooth. 

Hardware e materiale utilizzato:

* Arduino nano
* Modulo GY-521 MPU-6050
* DX2205 2300KV Brushless Motor 2CW 2CCW da 2-4S 
* Telaio a forma di x con Interasse: 220mm (Per supporto propelle fino a 5 pollici)
* 4 in 1 50A BLHeli ESC BLHeli 3-6s Supporto Lipo 
* Modulo Bluetooth hc 06
* Millefori 
* 4 Eliche da 5'
* Vari piedini maschio e femmina
* Batteria LiPo 14.8V 4S 1300mAh
* Condensatore da 470uF

*Link a repo* : https://gitlab.com/vladinc/bluetooth-drone

*Licenza scelta* : GPLv3

*Data presentazione*: Luglio


### The annoying alarm

*Nome Progetto*: The annoying alarm

*Autore*: Davide Rusconi

*Descrizione*: Il progetto consiste in una sveglia fatta a forma di macchina che, all'orario impostato dall'utente, si attiva ed inizia a muoversi in maniera autonoma all'interno dell'ambiente in cui è inserita evitando eventuali ostacoli che incontra.

*Link a repo*: https://github.com/psiquo/the-annoying-alarm

*Licenza Scelta*: GPLv3


### Arduino Robotic Hand

*Nome Progetto*: Arduino Robotic Hand

*Autore*: Matteo Cucchi

*Descrizione*: Il progetto consiste nella creazione di una mano meccanica in grado di mimare i movimenti compiuti dalla mano dell'utilizzatore mediante l'utilizzo di un guanto munito di sensori in grado di captare la flessione di ogni dito.

*Hardware*:
* Arduino Uno
* 5 sensori di flessione 4,5"
* 5 resistenze 10KΩ
* 5 servomotori MG995
* 2 connettori RJ45 (femmina)
* 1 cavo RJ45
* 1 breadboard
* 1 basetta millefori

*Link a repo* : https://gitlab.di.unimi.it/matteo.cucchi1/arduino-robotic-hand (In Allestimento: verra' modificato e riempito in fase di sviluppo)

*Licenza scelta* : GPLv3

### Automated Fan

Autore: Priscilla Scavo

Descrizione: Il progetto consiste nell’usare un servomotore che fa ruotare una ventola a seconda della posizione della persona che vi sta davanti. 
Il rilevamento della persona avviene attraverso il sensore HC-SR04, quando la distanza non verrà più rilevata significa che la persona si è spostata e allora il servomotore girerà finchè non la rileverà nuovamente. 
Oppure la posizione del braccio del servomotore può essere cambiata utilizzando un joystick. La potenza della ventola può essere impostata scegliendo tra tre modalità: bassa(3V), media(4V), alta(5V). 
Possono essere attivate due modalità aggiuntive: automatica (La velocità della ventola varia a seconda della temperatura nella stanza; la temperatura viene rilevata utilizzando il modulo GY-521 con comunicazione I2C) e da remoto (monitor seriale).

Hardware utilizzato:

  * Arduino UNO
  * 3 pulsanti
  * Motore DC da 3-6V
  * Servomotore microservo
  * Modulo HC-SR04
  * Batteria 9V
  * Joystick module
  * LCD1602 Module
  * GY-521

Link a repo:https://github.com/PriscillaScavo/Automated_Fan

Licenza scelta: Creative Commons Attribution 4.0 International

### TV Remote

Autore: Marco Cutecchia  

Descrizione: Un telecomando TV che, oltre alle funzionalità base, aggiunge:  
    - Un timer di spegnimento TV  
    - Un sistema di macro per ripetere azioni fatte da un altro telecomando infrarossi  
    - Traccia dati sull'utilizzo del telecomando e li manda via MQTT ad un server locale, oppure li salva su una scheda SD  
    - Comandi attivati con il movimento  

Link a repo: https://github.com/mrkct/tv-remote  

Licenza scelta: MIT  


### NOTA BENE: NON AGGIUNGERE I PROGETTI QUI IN FONDO, METTERLI NELLA SEZIONE "IN ITINERE"!!!
