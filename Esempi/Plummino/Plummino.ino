/* (atrent)
 * Plummino
 */

// TODO LOW splittare in più file

#define DEBUG

#include <TaskScheduler.h>

#include <Wire.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include "TinyGPS++.h"
TinyGPSPlus gps;
double latitude,longitude;
double latitudeToGo,longitudeToGo;
double courseToGo,distanceToGo;

//#include "MicroNMEA.h"
//char gpsbuffer[85];
//MicroNMEA nmea(gpsbuffer, sizeof(gpsbuffer));
//long latitude,longitude;
//long latitudeToGo,longitudeToGo;
//long nrOfSentences=0;
#define GPS_READ_DELAY 1

#include <SoftwareSerial.h>
SoftwareSerial swSer(D7, D8); //, false, 256);

/* Pass as a parameter the trigger and echo pin, respectively,
 * or only the signal pin (for sensors 3 pins), like:
 * Ultrasonic ultrasonic(13); */
#include <Ultrasonic.h>
Ultrasonic ultrasonic(D6, D5, 10000UL);
int obstacleDistance;
//#define MAXDISTANCE 50

#include "WEMOS_Motor.h"
long hold=0;
String command="";
#define MOTORS_CYCLE 100

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
/* Assign a unique ID to this sensor at the same time */
#define HMC5883_ADDRESS_MAG 0x1E
Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(999);
sensor_t sensor;
sensors_event_t event;
float headingDegrees;
//void compassDetails();
#define XCORRECTION 1
#define YCORRECTION 20

// Accelerometer
const int GY521=0x68; // I2C address
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

// surroundings TODO
#define SECTORS 8
float around[SECTORS];

// bumpers
#define COLLISION_LEFT D0
#define COLLISION_RIGHT D3
int collision_left,collision_right;

// switch
#define SWITCHPROG A0
int switchprog;

//#include <ESP8266mDNS.h>
////#include <ESP8266WiFiMulti.h>
////#include <ESP8266HTTPClient.h>
////#include <ArduinoOTA.h>
//#include <Arduino.h>


//Motor shield I2C Address: 0x30
//PWM frequency: 1000Hz(1kHz)
Motor *Mright = NULL; // right
Motor *Mleft  = NULL; // left

// motors
float rudder;
float speed;
int moving, left, right;

//#define ZEROING_TIME 10000
#define SENSITIVITY 0.1
#define ACCEL_SENSITIVITY 2
#define SILENCE		500

#define TOP_SPEED_R	70
#define TOP_SPEED_L	70
//#define TOP_SPEED_R	56
//#define TOP_SPEED_L	50

const byte MAX_MSG_SIZE PROGMEM=100;
byte packetBuffer[MAX_MSG_SIZE];  //buffer to hold incoming udp packet

////////////////////////////////// Tasks
//#define MOTORS
#define SERIAL
#define SERIALDRIVE
#define SENSORS
#define STRATEGY
#define GPS
#define IMALIVE
Scheduler runner;

#ifdef MOTORS
void callbackMotors();
Task taskMotors(MOTORS_CYCLE, TASK_FOREVER, &callbackMotors);
#endif

#ifdef SERIAL
void callbackStatusSerial();
Task taskStatusSerial(500, TASK_FOREVER, &callbackStatusSerial);
#endif

#ifdef SERIALDRIVE
void callbackSerialDrive();
Task taskSerialDrive(500, TASK_FOREVER, &callbackSerialDrive);
#endif

/*
void callbackMqtt();
Task taskMqtt(500, TASK_FOREVER, &callbackMqtt);
*/

#ifdef SENSORS
void callbackSensors();
Task taskSensors(25, TASK_FOREVER, &callbackSensors);
#endif

#ifdef GPS
void callbackGPS();
Task taskGPS(100, TASK_FOREVER, &callbackGPS);
#endif

#ifdef STRATEGY
void callbackStrategy();
Task taskStrategy(100, TASK_FOREVER, &callbackStrategy);
#endif

#ifdef IMALIVE
void callbackImAlive() {
    Serial.print("#");
}
Task taskImAlive(500, TASK_FOREVER, &callbackImAlive);
#endif

/////////////////////////////////////// wifi
//#include "wifi.h"
const String ssid = "SistEmbed";
const String pass = "8caratteri8"; // INSERIRE PASSWORD
const boolean STA PROGMEM=true;
//const boolean STA PROGMEM=false; // così fa da AP
WiFiUDP Udp;


/////////////////////////////////////////////////////
void setup() {
    pinMode(LED_BUILTIN,OUTPUT);
    collisionReset();

    //swSer.begin(4800);
    swSer.begin(9600);

    Serial.begin(115200);
    Serial.println("Setup...");

    if(STA)
        wifiSetupSTA();
    else
        wifiSetupAP();

    setupOTA();
    
    //setupDisplay();

    //setup ethernet part
    Udp.begin(7400);

    Wire.begin();
    // mando il messaggio di accensione
    Wire.beginTransmission(GY521);
    Wire.write(0x6B);  // registro PWR_MGMT_1
    Wire.write(0);     // se zero sveglia il chip MPU-6050
    Wire.endTransmission(true);


    Mright = new Motor(0x30,_MOTOR_A, 1000);//Motor A
    Mleft  = new Motor(0x30,_MOTOR_B, 1000);//Motor B


    /* Initialise the compass */
    if(!mag.begin()) {
        /* There was a problem detecting the HMC5883 ... check your connections */
        Serial.println("Ooops, no HMC5883 detected ... Check your wiring!");
        while(1);
    }

    /* Display some basic information on this sensor */
    //compassDetails();


    // Tasks
    runner.init();

#ifdef MOTORS
    runner.addTask(taskMotors);
    taskMotors.enable();
#endif

#ifdef SERIAL
    runner.addTask(taskStatusSerial);
    taskStatusSerial.enable();
#endif

#ifdef SERIALDRIVE
    runner.addTask(taskSerialDrive);
    taskSerialDrive.enable();
#endif

#ifdef SENSORS
    runner.addTask(taskSensors);
    taskSensors.enable();
#endif

#ifdef STRATEGY
    runner.addTask(taskStrategy);
    taskStrategy.enable();
#endif

#ifdef GPS
    runner.addTask(taskGPS);
    taskGPS.enable();
#endif

#ifdef IMALIVE
    runner.addTask(taskImAlive);
    taskImAlive.enable();
#endif

    Serial.println("End of setup...");
}

void loop() {
    //Serial.println("loop");
    runner.execute();
    ArduinoOTA.handle();
}

void pingLed() {
    digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)
    delay(10);
    digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
}

void callbackSensors() {
    /////////////////////////////////////
    // COMPASS
	//Serial.println("pre bussola");
    mag.getSensor(&sensor);
    mag.getEvent(&event);
	//Serial.println("post bussola");

    // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
    // Calculate heading when the magnetometer is level, then correct for signs of axis.
    float heading = atan2(event.magnetic.y+YCORRECTION, event.magnetic.x+XCORRECTION);

    // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
    // Find yours here: http://www.magnetic-declination.com/
    // Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
    // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
    float declinationAngle = 0.04; // corretto per milano
    heading += declinationAngle;

    // Correct for when signs are reversed.
    if(heading < 0)
        heading += 2*PI;

    // Check for wrap due to addition of declination.
    if(heading > 2*PI)
        heading -= 2*PI;

    // Convert radians to degrees for readability.
    headingDegrees = heading * 180/M_PI;


    ////////////////////////////////////////////
    // Switch TODO
    switchprog=analogRead(SWITCHPROG);

    ////////////////////////////////////////////
    // Collision
    collision_right=digitalRead(COLLISION_RIGHT);
    collision_left=digitalRead(COLLISION_LEFT);
    collisionReset();

    ////////////////////////////////////////////
    // Distance
    obstacleDistance=ultrasonic.distanceRead();
    //if(distance>MAXDISTANCE) distance=MAXDISTANCE;
    //Serial.println(distance);

    // call gps
    //callbackGPS();

    ////////////////////////////////////////////
    // Accelerometro
    readAccelerometer();
}

void callbackGPS() {
    ////////////////////////////////////////////
    // GPS
    //Serial.println("gps...");

    // TinyGps++
    while (swSer.available()) {
        char c=swSer.read();
        gps.encode(c);
#ifdef DEBUG
        Serial.print(c);
#endif
        delay(GPS_READ_DELAY);
    }

    latitude=gps.location.lat();
    longitude=gps.location.lng();

    /* MicroNMEA
    while(swSer.available()) {
        char c = swSer.read();
        //Serial.print(c);
        if (nmea.process(c)) {
            nrOfSentences++;
            //Serial.println(nmea.getSentence());
            //Serial.println(nrOfSentences);
            // Complete NMEA command read and processed, do something
            latitude=nmea.getLatitude();
            longitude=nmea.getLongitude();
            pingLed();
        }
        delay(GPS_READ_DELAY);
    }
    */

    if(switchprog>1000) {
        latitudeToGo=latitude;
        longitudeToGo=longitude;
    }

    courseToGo =
        TinyGPSPlus::courseTo(
            latitude,
            longitude,
            latitudeToGo,
            longitudeToGo);


    distanceToGo=TinyGPSPlus::distanceBetween(
                     latitude,
                     longitude,
                     latitudeToGo,
                     longitudeToGo) / 1000;

}

void callbackSerialDrive() {
    if(Serial.available()) {
        command=Serial.readStringUntil(' ');
        hold=Serial.parseInt();
        while(Serial.available()) {
            Serial.read();
        };
    }
}

void callbackStatusSerial() {
    Serial.println("------------------------------------");
    //Serial.println(millis());

    /* da OSC
    Serial.print("gX:");
    Serial.print(gX);
    Serial.print("/");
    Serial.print(gXbase);

    Serial.print("\tgY:");
    Serial.print(gY);
    Serial.print("/");
    Serial.print(gYbase);

    Serial.print("\tgZ:");
    Serial.print(gZ);
    Serial.print("/");
    Serial.println(gZbase);

    Serial.print("accelX: ");
    Serial.print(aX);

    Serial.print("\taccelY: ");
    Serial.print(aY);

    Serial.print("\taccelZ: ");
    Serial.println(aZ);

    Serial.print("Rudder: ");
    Serial.print(rudder);

    Serial.print("\tSpeed: ");
    Serial.println(speed);
    // end OSC
    */

    Serial.print("Sonar distance: ");
    Serial.println(obstacleDistance);

    Serial.print("Collision: left ");
    Serial.print(collision_left);
    Serial.print(" right ");
    Serial.println(collision_right);

    Serial.print("Switch: ");
    Serial.println(switchprog);

    /*
    Serial.print("GPS sentences: ");
    Serial.print(nrOfSentences);
    Serial.print(",");
    Serial.println(nmea.getSentence());
    */

    Serial.print("Accelerometer:\t");
    Serial.print(AcX);
    Serial.print(",");
    Serial.print(AcY);
    Serial.print(",");
    Serial.print(AcZ);
    Serial.print("\t");
    Serial.print(Tmp);
    Serial.print("\t");
    Serial.print(GyX);
    Serial.print(",");
    Serial.print(GyY);
    Serial.print(",");
    Serial.println(GyZ);

    printLatLon();
    compassDetails();
    //pingLed();

    Serial.print("Last command: ");
    Serial.print(command);
    Serial.print("\tfor ");
    Serial.println(hold);
}

void printLatLon() {
    Serial.print("Lat: ");
    Serial.print(latitude);
    Serial.print(", Lon: ");
    Serial.print(longitude);

    Serial.print("\tLat2go: ");
    Serial.print(latitudeToGo);
    Serial.print(", Lon2go: ");
    Serial.println(longitudeToGo);

    Serial.print("bearingToGo: ");
    Serial.print(courseToGo);
    Serial.print("\tdistanceToGo: ");
    Serial.println(distanceToGo);
}


//////////////////////////////////

#define ARM 5000
void callbackStrategy() {
    /*
    long m=millis()%(4*ARM);
    if(m>=0 && m<ARM) courseToGo=0;
    if(m>=ARM && m<2*ARM) courseToGo=90;
    if(m>=2*ARM && m<3*ARM) courseToGo=180;
    if(m>=3*ARM && m<4*ARM) courseToGo=270;
    */

    // TEST
    courseToGo=252;

    if(switchprog>1000) {
        //Serial.print("m:");
        //Serial.println(m);
        //Serial.print("course:");
        //Serial.println(courseToGo);

        rudder=(courseToGo-headingDegrees)/50;
        moving=_CCW;
        setRLfromRudder();
        setMotors();
    }

    /*
    if(distanceToGo>0.001 && switchprog<1000) {
        Serial.println("strategy...");
        pingLed();
        rudder=(courseToGo-headingDegrees)/10;
        moving=_CCW;
        setRLfromRudder();
        setMotors();
    }
    */
    else
        fullStop();
}

void callbackMotors() {
    // pochi comandi per fare qualche demo

    int ml=0,mr=0;

    // AVANTI
    if(command.equals("AVANTI")) {
        ml=mr=_CCW;
    }

    // INDIETRO
    if(command.equals("INDIETRO")) {
        ml=mr=_CW;
    }

    // RUOTADX
    if(command.equals("RUOTADX")) {
        mr=_CCW;
        ml=_CW;
    }

    // RUOTASX
    if(command.equals("RUOTASX")) {
        mr=_CW;
        ml=_CCW;
    }

    if(hold>0) {
        hold-=MOTORS_CYCLE;
        Mright->setmotor(mr, TOP_SPEED_R);
        Mleft->setmotor(ml, TOP_SPEED_L);
    } else {
        // stop everything
        Mright->setmotor(_STOP);
        Mleft->setmotor(_STOP);
    }
}

////////////////////////////////// WIFI
void wifiSetupSTA() {
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid.c_str(), pass.c_str());

    Serial.print("Connecting...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println();

    Serial.print("Connected, IP address: ");
    Serial.println(WiFi.localIP());
}

void wifiSetupAP() {
    WiFi.mode(WIFI_AP);

    WiFi.softAP(ssid.c_str());
    Serial.print("Started, IP address: ");
    Serial.println(WiFi.softAPIP());
}

void compassDetails() {
    //mag.getSensor(&sensor);
    /*
    Serial.println("------------------------------------");
    Serial.print  ("Sensor:       ");
    Serial.print(sensor.name);
    Serial.print  ("Driver Ver:   ");
    Serial.println(sensor.version);
    Serial.print  ("Unique ID:    ");
    Serial.println(sensor.sensor_id);
    Serial.print  ("Max Value:    ");
    Serial.print(sensor.max_value);
    Serial.println(" uT");
    Serial.print  ("Min Value:    ");
    Serial.print(sensor.min_value);
    Serial.println(" uT");
    Serial.print  ("Resolution:   ");
    Serial.print(sensor.resolution);
    Serial.println(" uT");
    Serial.println("------------------------------------");
    */
    Serial.print("Heading: ");
    Serial.print(headingDegrees);
    Serial.print("\t(X: ");
    Serial.print(event.magnetic.x);
    Serial.print("  ");
    Serial.print("Y: ");
    Serial.print(event.magnetic.y);
    Serial.print("  ");
    Serial.print("Z: ");
    Serial.print(event.magnetic.z);
    Serial.print("  ");
    Serial.println("uT)");
    //Serial.println("------------------------------------");
}

void collisionReset() {
	// strano dover fare 'sta cosa, se no non tornano su, mah!
    pinMode(COLLISION_LEFT,OUTPUT);
    pinMode(COLLISION_RIGHT,OUTPUT);
    digitalWrite(COLLISION_LEFT,HIGH);
    digitalWrite(COLLISION_RIGHT,HIGH);
    pinMode(COLLISION_LEFT,INPUT_PULLUP);
    pinMode(COLLISION_RIGHT,INPUT_PULLUP);
    // tra l'altro solo il left, ri-mah!
}

// TODO trovare lib che faccia un po' di smoothing e integrazione
void readAccelerometer() {
    // mando il messaggio di richiesta valori
    Wire.beginTransmission(GY521);
    Wire.write(0x3B);  // a partire dal registro 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission(false);
    Wire.requestFrom(GY521,14,true);  // in tutto 14 registri
    AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}


void setMotors() {
    Mright->setmotor(moving, right);
    Mleft->setmotor(moving, left);

    //Serial.print(right);
    //Serial.print(",");
    //Serial.println(left);
}

void setRLfromRudder() {
    right = map(rudder, 10, -10, 1, TOP_SPEED_R);
    left = map(rudder, -10, 10, 1, TOP_SPEED_L);
}


void fullStop() {
    Mright->setmotor(_STOP);
    Mleft->setmotor(_STOP);
}




/* avanzi motori
  M1->setmotor(_STOP);
  M2->setmotor( _STOP);

  M1->setmotor(_SHORT_BRAKE);
  M2->setmotor( _SHORT_BRAKE);

  M1->setmotor(_STANDBY);//Both Motor standby
  M2.setmotor( _STANDBY);
*/

/* avanzi OTA
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");
*/




void setupOTA() {
    // Port defaults to 8266
    // ArduinoOTA.setPort(8266);

    // Hostname defaults to esp8266-[ChipID]
    ArduinoOTA.setHostname("Plummino");

    // No authentication by default
    //ArduinoOTA.setPassword("plum");

    // Password can be set with it's md5 value as well
    // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
    // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
            type = "sketch";
        } else { // U_FS
            type = "filesystem";
        }

        // NOTE: if updating FS this would be the place to unmount FS using FS.end()
        Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
    });

    ArduinoOTA.begin();
    Serial.println("OTA Ready");
}
